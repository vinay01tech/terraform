# Steps to execute terraform

Below command is one time step to load provider plugin inside the project.  
`terraform init`

# Terraform commands:

To execute plan and verify your changes  
`terraform plan`

To apply the changes  
`terraform apply`

To destroy the changes  
`terraform destroy`

