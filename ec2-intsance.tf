resource "aws_instance" "ec2_instance" {
  
  # It's a ubuntu 18 ami, can be replace with the ami of any machine type like centos, ubuntu 16 etc.
  ami = "ami-01e6a0b85de033c99"
  
  instance_type = "t2.micro"
  
  # key generated for the account
  key_name = "demo"

  tags = {
    Name = "demo-instance"
  }
}